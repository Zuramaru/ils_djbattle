// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32719,y:32712,varname:node_4013,prsc:2|diff-3805-OUT,emission-7637-OUT,difocc-1467-OUT;n:type:ShaderForge.SFN_Tex2d,id:614,x:31991,y:32391,ptovrint:False,ptlb:Color (RGB) AO (A),ptin:_ColorRGBAOA,varname:node_614,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:915bd67841b21f846852441fef8f7352,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:8079,x:31947,y:32953,ptovrint:False,ptlb:Glow,ptin:_Glow,varname:node_8079,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:4912b48d147792b49a594a3a12e2da68,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7637,x:32399,y:32893,varname:node_7637,prsc:2|A-8079-RGB,B-943-OUT,C-7650-OUT;n:type:ShaderForge.SFN_Multiply,id:3805,x:32352,y:32488,varname:node_3805,prsc:2|A-614-RGB,B-6222-RGB;n:type:ShaderForge.SFN_Slider,id:1879,x:31892,y:32787,ptovrint:False,ptlb:AO intensity,ptin:_AOintensity,varname:node_1879,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-3,cur:3,max:3;n:type:ShaderForge.SFN_Add,id:1467,x:32352,y:32722,varname:node_1467,prsc:2|A-614-A,B-1879-OUT;n:type:ShaderForge.SFN_Color,id:6222,x:31991,y:32605,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6222,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.8088235,c2:0.6440237,c3:0.5174092,c4:1;n:type:ShaderForge.SFN_Sin,id:8758,x:31471,y:33121,varname:node_8758,prsc:2|IN-8635-OUT;n:type:ShaderForge.SFN_Time,id:1955,x:31073,y:33059,varname:node_1955,prsc:2;n:type:ShaderForge.SFN_Slider,id:6241,x:30916,y:33266,ptovrint:False,ptlb:Glow Intermittence Speed,ptin:_GlowIntermittenceSpeed,varname:node_6241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.35,max:1;n:type:ShaderForge.SFN_Multiply,id:8635,x:31286,y:33133,varname:node_8635,prsc:2|A-1955-T,B-6241-OUT;n:type:ShaderForge.SFN_Slider,id:7650,x:31947,y:33322,ptovrint:False,ptlb:Glow Intensity,ptin:_GlowIntensity,varname:node_7650,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.5,max:1;n:type:ShaderForge.SFN_Exp,id:510,x:31673,y:33111,varname:node_510,prsc:2,et:1|IN-8758-OUT;n:type:ShaderForge.SFN_Exp,id:943,x:31947,y:33131,varname:node_943,prsc:2,et:0|IN-510-OUT;proporder:6222-614-8079-1879-7650-6241;pass:END;sub:END;*/

Shader "Shader Forge/SimpleLit_w_Glow" {
    Properties {
        _Color ("Color", Color) = (0.8088235,0.6440237,0.5174092,1)
        _ColorRGBAOA ("Color (RGB) AO (A)", 2D) = "white" {}
        _Glow ("Glow", 2D) = "white" {}
        _AOintensity ("AO intensity", Range(-3, 3)) = 3
        _GlowIntensity ("Glow Intensity", Range(0, 1)) = 0.5
        _GlowIntermittenceSpeed ("Glow Intermittence Speed", Range(0, 1)) = 0.35
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _ColorRGBAOA; uniform float4 _ColorRGBAOA_ST;
            uniform sampler2D _Glow; uniform float4 _Glow_ST;
            uniform float _AOintensity;
            uniform float4 _Color;
            uniform float _GlowIntermittenceSpeed;
            uniform float _GlowIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _ColorRGBAOA_var = tex2D(_ColorRGBAOA,TRANSFORM_TEX(i.uv0, _ColorRGBAOA));
                indirectDiffuse *= (_ColorRGBAOA_var.a+_AOintensity); // Diffuse AO
                float3 diffuseColor = (_ColorRGBAOA_var.rgb*_Color.rgb);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float4 _Glow_var = tex2D(_Glow,TRANSFORM_TEX(i.uv0, _Glow));
                float4 node_1955 = _Time + _TimeEditor;
                float node_8758 = sin((node_1955.g*_GlowIntermittenceSpeed));
                float node_510 = exp2(node_8758);
                float node_943 = exp(node_510);
                float3 emissive = (_Glow_var.rgb*node_943*_GlowIntensity);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _TimeEditor;
            uniform sampler2D _ColorRGBAOA; uniform float4 _ColorRGBAOA_ST;
            uniform sampler2D _Glow; uniform float4 _Glow_ST;
            uniform float4 _Color;
            uniform float _GlowIntermittenceSpeed;
            uniform float _GlowIntensity;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _ColorRGBAOA_var = tex2D(_ColorRGBAOA,TRANSFORM_TEX(i.uv0, _ColorRGBAOA));
                float3 diffuseColor = (_ColorRGBAOA_var.rgb*_Color.rgb);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

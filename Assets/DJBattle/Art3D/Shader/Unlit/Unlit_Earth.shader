// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-8656-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31998,y:32919,ptovrint:False,ptlb:Rim Intensity Color,ptin:_RimIntensityColor,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.9586205,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:4309,x:31709,y:32600,ptovrint:False,ptlb:Diffuse,ptin:_Diffuse,varname:node_4309,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fd704039f701e2e4f9923dca24ec23e9,ntxv:0,isnm:False|UVIN-6458-UVOUT;n:type:ShaderForge.SFN_Add,id:5935,x:32097,y:32721,varname:node_5935,prsc:2|A-4309-RGB,B-1733-OUT;n:type:ShaderForge.SFN_Slider,id:2207,x:31535,y:32867,ptovrint:False,ptlb:Texture Intensity,ptin:_TextureIntensity,varname:node_2207,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.6,max:1;n:type:ShaderForge.SFN_Multiply,id:1733,x:31899,y:32768,varname:node_1733,prsc:2|A-4309-RGB,B-2207-OUT;n:type:ShaderForge.SFN_Fresnel,id:3993,x:31998,y:33088,varname:node_3993,prsc:2|EXP-7349-OUT;n:type:ShaderForge.SFN_Slider,id:7349,x:31559,y:33020,ptovrint:False,ptlb:Rim Effect,ptin:_RimEffect,varname:node_7349,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.316377,max:5;n:type:ShaderForge.SFN_Add,id:8656,x:32503,y:32784,varname:node_8656,prsc:2|A-5935-OUT,B-1823-OUT;n:type:ShaderForge.SFN_Panner,id:6458,x:31444,y:32605,varname:node_6458,prsc:2,spu:1,spv:0|UVIN-270-UVOUT,DIST-8702-OUT;n:type:ShaderForge.SFN_TexCoord,id:270,x:31193,y:32542,varname:node_270,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Slider,id:804,x:30811,y:32760,ptovrint:False,ptlb:RotationSpeed,ptin:_RotationSpeed,varname:node_804,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.2,max:1;n:type:ShaderForge.SFN_Multiply,id:8702,x:31228,y:32807,varname:node_8702,prsc:2|A-804-OUT,B-5527-TSL;n:type:ShaderForge.SFN_Time,id:5527,x:30968,y:32877,varname:node_5527,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1823,x:32307,y:32843,varname:node_1823,prsc:2|A-7241-RGB,B-3993-OUT;proporder:4309-2207-7241-7349-804;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Earth" {
    Properties {
        _Diffuse ("Diffuse", 2D) = "white" {}
        _TextureIntensity ("Texture Intensity", Range(0, 1)) = 0.6
        _RimIntensityColor ("Rim Intensity Color", Color) = (0,0.9586205,1,1)
        _RimEffect ("Rim Effect", Range(0, 5)) = 1.316377
        _RotationSpeed ("RotationSpeed", Range(0, 1)) = 0.2
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _RimIntensityColor;
            uniform sampler2D _Diffuse; uniform float4 _Diffuse_ST;
            uniform float _TextureIntensity;
            uniform float _RimEffect;
            uniform float _RotationSpeed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_5527 = _Time + _TimeEditor;
                float2 node_6458 = (i.uv0+(_RotationSpeed*node_5527.r)*float2(1,0));
                float4 _Diffuse_var = tex2D(_Diffuse,TRANSFORM_TEX(node_6458, _Diffuse));
                float3 emissive = ((_Diffuse_var.rgb+(_Diffuse_var.rgb*_TextureIntensity))+(_RimIntensityColor.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),_RimEffect)));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

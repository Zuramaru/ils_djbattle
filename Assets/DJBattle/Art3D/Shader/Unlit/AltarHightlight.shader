// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-2028-OUT,alpha-5440-A,clip-5440-A;n:type:ShaderForge.SFN_TexCoord,id:5139,x:31642,y:32467,varname:node_5139,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Time,id:7140,x:31461,y:32665,varname:node_7140,prsc:2;n:type:ShaderForge.SFN_Slider,id:5992,x:31304,y:32864,ptovrint:False,ptlb:Scroll Speed,ptin:_ScrollSpeed,varname:node_5992,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.5,max:10;n:type:ShaderForge.SFN_Tex2d,id:5440,x:32193,y:32510,ptovrint:False,ptlb:Glow Texture,ptin:_GlowTexture,varname:node_5440,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:eb8dd60636eb07e45a991976b68a862d,ntxv:0,isnm:False|UVIN-5526-UVOUT;n:type:ShaderForge.SFN_Panner,id:5526,x:31972,y:32461,varname:node_5526,prsc:2,spu:0,spv:-0.2|UVIN-5139-UVOUT,DIST-5634-OUT;n:type:ShaderForge.SFN_Multiply,id:5634,x:31737,y:32693,varname:node_5634,prsc:2|A-7140-T,B-5992-OUT;n:type:ShaderForge.SFN_Multiply,id:2028,x:32473,y:32559,varname:node_2028,prsc:2|A-5440-RGB,B-6738-RGB;n:type:ShaderForge.SFN_Color,id:6738,x:32193,y:32744,ptovrint:False,ptlb:Glow Color,ptin:_GlowColor,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.0344826,c2:1,c3:0,c4:1;proporder:6738-5440-5992;pass:END;sub:END;*/

Shader "Shader Forge/AltarHightlight" {
    Properties {
        _GlowColor ("Glow Color", Color) = (0.0344826,1,0,1)
        _GlowTexture ("Glow Texture", 2D) = "white" {}
        _ScrollSpeed ("Scroll Speed", Range(0, 10)) = 2.5
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _ScrollSpeed;
            uniform sampler2D _GlowTexture; uniform float4 _GlowTexture_ST;
            uniform float4 _GlowColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_7140 = _Time + _TimeEditor;
                float2 node_5526 = (i.uv0+(node_7140.g*_ScrollSpeed)*float2(0,-0.2));
                float4 _GlowTexture_var = tex2D(_GlowTexture,TRANSFORM_TEX(node_5526, _GlowTexture));
                clip(_GlowTexture_var.a - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = (_GlowTexture_var.rgb*_GlowColor.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,_GlowTexture_var.a);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _ScrollSpeed;
            uniform sampler2D _GlowTexture; uniform float4 _GlowTexture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 node_7140 = _Time + _TimeEditor;
                float2 node_5526 = (i.uv0+(node_7140.g*_ScrollSpeed)*float2(0,-0.2));
                float4 _GlowTexture_var = tex2D(_GlowTexture,TRANSFORM_TEX(node_5526, _GlowTexture));
                clip(_GlowTexture_var.a - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.36 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.36;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33108,y:32704,varname:node_3138,prsc:2|emission-2281-OUT,clip-5562-OUT;n:type:ShaderForge.SFN_Color,id:4975,x:31820,y:32230,ptovrint:False,ptlb:Top Color,ptin:_TopColor,varname:node_4975,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_ObjectPosition,id:1522,x:31007,y:32920,varname:node_1522,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:619,x:31007,y:33077,varname:node_619,prsc:2;n:type:ShaderForge.SFN_Subtract,id:6058,x:31229,y:33024,varname:node_6058,prsc:2|A-1522-XYZ,B-619-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:3931,x:31407,y:33024,varname:node_3931,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-6058-OUT;n:type:ShaderForge.SFN_Add,id:5041,x:31622,y:33024,varname:node_5041,prsc:2|A-3931-OUT,B-9756-OUT;n:type:ShaderForge.SFN_Vector3,id:4609,x:31007,y:33288,varname:node_4609,prsc:2,v1:0,v2:1,v3:0;n:type:ShaderForge.SFN_ComponentMask,id:9756,x:31217,y:33288,varname:node_9756,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4609-OUT;n:type:ShaderForge.SFN_Slider,id:1508,x:30623,y:33461,ptovrint:False,ptlb:Liquid Height,ptin:_LiquidHeight,varname:node_1508,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:125,x:31410,y:33298,varname:node_125,prsc:2|A-9756-OUT,B-8500-OUT;n:type:ShaderForge.SFN_Subtract,id:5097,x:31820,y:33065,varname:node_5097,prsc:2|A-5041-OUT,B-8685-OUT;n:type:ShaderForge.SFN_Divide,id:1682,x:32016,y:33065,varname:node_1682,prsc:2|A-5097-OUT,B-1604-OUT;n:type:ShaderForge.SFN_Vector1,id:1604,x:31820,y:33327,varname:node_1604,prsc:2,v1:0;n:type:ShaderForge.SFN_Clamp01,id:5562,x:32195,y:33065,varname:node_5562,prsc:2|IN-1682-OUT;n:type:ShaderForge.SFN_Subtract,id:6750,x:31043,y:33472,varname:node_6750,prsc:2|A-1508-OUT,B-616-OUT;n:type:ShaderForge.SFN_Vector1,id:616,x:30797,y:33602,varname:node_616,prsc:2,v1:1;n:type:ShaderForge.SFN_Abs,id:8500,x:31217,y:33472,varname:node_8500,prsc:2|IN-6750-OUT;n:type:ShaderForge.SFN_RemapRange,id:8685,x:31609,y:33454,varname:node_8685,prsc:2,frmn:0,frmx:1,tomn:0,tomx:2|IN-125-OUT;n:type:ShaderForge.SFN_Lerp,id:8374,x:32154,y:32598,varname:node_8374,prsc:2|A-9680-OUT,B-2776-OUT,T-156-OUT;n:type:ShaderForge.SFN_Color,id:3207,x:31692,y:32608,ptovrint:False,ptlb:Base Color,ptin:_BaseColor,varname:node_3207,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.8460446,c3:0.5147059,c4:1;n:type:ShaderForge.SFN_FaceSign,id:2920,x:32154,y:32780,varname:node_2920,prsc:2,fstp:0;n:type:ShaderForge.SFN_Lerp,id:3383,x:32442,y:32601,varname:node_3383,prsc:2|A-9680-OUT,B-8374-OUT,T-2920-VFACE;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:156,x:31820,y:32841,varname:node_156,prsc:2|IN-5041-OUT,IMIN-2140-OUT,IMAX-5074-OUT,OMIN-2946-OUT,OMAX-9678-OUT;n:type:ShaderForge.SFN_Slider,id:9678,x:31250,y:32881,ptovrint:False,ptlb:Gradient Max,ptin:_GradientMax,varname:_node_1838_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-5,cur:0.5,max:5;n:type:ShaderForge.SFN_Slider,id:2946,x:31250,y:32782,ptovrint:False,ptlb:Gradient Min,ptin:_GradientMin,varname:_node_1838_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-5,cur:-2,max:5;n:type:ShaderForge.SFN_Vector1,id:2140,x:31407,y:32603,varname:node_2140,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Slider,id:5074,x:31250,y:32689,ptovrint:False,ptlb:Gradient Offset,ptin:_GradientOffset,varname:_GradientMin_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-5,cur:1.095656,max:5;n:type:ShaderForge.SFN_Tex2dAsset,id:1069,x:31044,y:32054,ptovrint:False,ptlb:Bubbles Texture,ptin:_BubblesTexture,varname:node_1069,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1194,x:31286,y:31957,varname:node_1194,prsc:2,tex:70dd85c2940616742a36a6b708c7b614,ntxv:0,isnm:False|UVIN-3603-OUT,TEX-1069-TEX;n:type:ShaderForge.SFN_Time,id:3549,x:30314,y:31909,varname:node_3549,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:8295,x:30572,y:31797,varname:node_8295,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Add,id:9326,x:30815,y:31912,varname:node_9326,prsc:2|A-8295-V,B-4275-OUT;n:type:ShaderForge.SFN_Append,id:3603,x:31045,y:31814,varname:node_3603,prsc:2|A-8295-U,B-9326-OUT;n:type:ShaderForge.SFN_Multiply,id:4275,x:30573,y:32011,varname:node_4275,prsc:2|A-3549-TSL,B-3924-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3924,x:30314,y:32109,ptovrint:False,ptlb:Bubbles Speed,ptin:_BubblesSpeed,varname:node_3924,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-20;n:type:ShaderForge.SFN_RemapRange,id:2903,x:31549,y:31961,varname:node_2903,prsc:2,frmn:0,frmx:1,tomn:0,tomx:2|IN-1194-R;n:type:ShaderForge.SFN_Multiply,id:2159,x:31818,y:31969,varname:node_2159,prsc:2|A-3604-OUT,B-5143-OUT;n:type:ShaderForge.SFN_Color,id:4366,x:31301,y:32176,ptovrint:False,ptlb:Bubbles Color,ptin:_BubblesColor,varname:_node_6523_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_OneMinus,id:5143,x:31549,y:32176,varname:node_5143,prsc:2|IN-4366-RGB;n:type:ShaderForge.SFN_OneMinus,id:5445,x:32061,y:31969,varname:node_5445,prsc:2|IN-2159-OUT;n:type:ShaderForge.SFN_Multiply,id:9680,x:32061,y:32175,varname:node_9680,prsc:2|A-5445-OUT,B-4975-RGB;n:type:ShaderForge.SFN_SwitchProperty,id:5923,x:32646,y:32512,ptovrint:False,ptlb:Bubbles,ptin:_Bubbles,varname:node_5923,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:True|A-2776-OUT,B-3383-OUT;n:type:ShaderForge.SFN_Color,id:8644,x:31692,y:32435,ptovrint:False,ptlb:Tint Color,ptin:_TintColor,varname:node_8644,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:2776,x:31889,y:32507,varname:node_2776,prsc:2|A-8644-RGB,B-3207-RGB;n:type:ShaderForge.SFN_Cubemap,id:4171,x:32646,y:32757,ptovrint:False,ptlb:Cubemap,ptin:_Cubemap,varname:node_4171,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,cube:2a3ec67763a393546a96a22f60815b96,pvfc:0;n:type:ShaderForge.SFN_Add,id:2281,x:32867,y:32656,varname:node_2281,prsc:2|A-5923-OUT,B-4171-RGB;n:type:ShaderForge.SFN_Noise,id:1006,x:30820,y:31538,varname:node_1006,prsc:2|XY-8295-UVOUT;n:type:ShaderForge.SFN_Multiply,id:3604,x:31559,y:31647,varname:node_3604,prsc:2|A-1006-OUT,B-2903-OUT;proporder:1508-8644-4366-4975-3207-5074-9678-2946-4171-1069-3924-5923;pass:END;sub:END;*/

Shader "Shader Forge/Unlit_Liquid" {
    Properties {
        _LiquidHeight ("Liquid Height", Range(0, 1)) = 1
        _TintColor ("Tint Color", Color) = (1,0,0,1)
        _BubblesColor ("Bubbles Color", Color) = (1,0,0,1)
        _TopColor ("Top Color", Color) = (1,1,1,1)
        _BaseColor ("Base Color", Color) = (1,0.8460446,0.5147059,1)
        _GradientOffset ("Gradient Offset", Range(-5, 5)) = 1.095656
        _GradientMax ("Gradient Max", Range(-5, 5)) = 0.5
        _GradientMin ("Gradient Min", Range(-5, 5)) = -2
        _Cubemap ("Cubemap", Cube) = "_Skybox" {}
        _BubblesTexture ("Bubbles Texture", 2D) = "white" {}
        _BubblesSpeed ("Bubbles Speed", Float ) = -20
        [MaterialToggle] _Bubbles ("Bubbles", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _TopColor;
            uniform float _LiquidHeight;
            uniform float4 _BaseColor;
            uniform float _GradientMax;
            uniform float _GradientMin;
            uniform float _GradientOffset;
            uniform sampler2D _BubblesTexture; uniform float4 _BubblesTexture_ST;
            uniform float _BubblesSpeed;
            uniform float4 _BubblesColor;
            uniform fixed _Bubbles;
            uniform float4 _TintColor;
            uniform samplerCUBE _Cubemap;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float node_9756 = float3(0,1,0).g;
                float node_5041 = ((objPos.rgb-i.posWorld.rgb).g+node_9756);
                clip(saturate(((node_5041-((node_9756*abs((_LiquidHeight-1.0)))*2.0+0.0))/0.0)) - 0.5);
////// Lighting:
////// Emissive:
                float3 node_2776 = (_TintColor.rgb*_BaseColor.rgb);
                float2 node_1006_skew = i.uv0 + 0.2127+i.uv0.x*0.3713*i.uv0.y;
                float2 node_1006_rnd = 4.789*sin(489.123*(node_1006_skew));
                float node_1006 = frac(node_1006_rnd.x*node_1006_rnd.y*(1+node_1006_skew.x));
                float4 node_3549 = _Time + _TimeEditor;
                float2 node_3603 = float2(i.uv0.r,(i.uv0.g+(node_3549.r*_BubblesSpeed)));
                float4 node_1194 = tex2D(_BubblesTexture,TRANSFORM_TEX(node_3603, _BubblesTexture));
                float3 node_9680 = ((1.0 - ((node_1006*(node_1194.r*2.0+0.0))*(1.0 - _BubblesColor.rgb)))*_TopColor.rgb);
                float node_2140 = 0.8;
                float3 emissive = (lerp( node_2776, lerp(node_9680,lerp(node_9680,node_2776,(_GradientMin + ( (node_5041 - node_2140) * (_GradientMax - _GradientMin) ) / (_GradientOffset - node_2140))),isFrontFace), _Bubbles )+texCUBE(_Cubemap,viewReflectDirection).rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _LiquidHeight;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 posWorld : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                float node_9756 = float3(0,1,0).g;
                float node_5041 = ((objPos.rgb-i.posWorld.rgb).g+node_9756);
                clip(saturate(((node_5041-((node_9756*abs((_LiquidHeight-1.0)))*2.0+0.0))/0.0)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

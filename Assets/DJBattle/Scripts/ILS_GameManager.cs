﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_GameManager : MonoBehaviour
{
    public ILS_ConsoleBehavior console;
    public ILS_SongManager songManager;

    void Start()
    {
        console.Initialize();
        console.EnableConsole(ConsoleStates.Blue);
        StartCoroutine(StartGame());
    }

    public void ActiveAnimation()
    {
        Debug.Log("Elimboke");
        songManager.StartSong();
        //songManager.SendAnimationDisc();
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(3f);
        ActiveAnimation();
    }
    
}

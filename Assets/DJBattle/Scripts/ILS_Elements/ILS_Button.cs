﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class ILS_Button : ILS_ElementBehavior
{
    private bool CD = true;
    private GameObject child;
    private float actualTime = 0.1f;
    public AudioSource Tap;

    private void Awake()
    {
        base.Initialize();
        child = transform.GetChild(0).gameObject;
    }

    public void ActiveAnimation(float lenght)
    {
        StartCoroutine(ButtonCD(lenght));
        LeanTween.scale(child, new Vector3(0.01f, 0.01f, 0.01f), lenght).setOnComplete(EnableButton);
    }

    private void EnableButton()
    {
        child.transform.localScale = new Vector3(0.02533807f, 0.0253043f, 0.3740521f);
    }

    public void IsTouch()
    {
        Debug.Log("Tap");
        CD = false;
        Tap.Play();
    }

    IEnumerator ButtonCD(float lenght) {
        float valueToHit = 0;
        while (valueToHit < lenght)
        {
            Debug.Log("Es menor");
            valueToHit += 0.1f;
            yield return new WaitForSeconds(actualTime);
        }
        if (CD)
        {
            MinusScore();
            valueToHit = 0;
           //Debug.Log("Fallo");
        }
        if(!CD)
        {
            PlusScore();
            valueToHit = 0;
            Debug.Log("Anotacion");
            CD = true;
        }
    }

    public override void PlusScore()
    {
        base.PlusScore();
    }

    public override void MinusScore()
    {
        base.MinusScore();
    }
}

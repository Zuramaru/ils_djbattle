﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ILS_ElementBehavior : MonoBehaviour
{
    protected ILS_UIScore UIScore;

    //private void Update()
    //{
    //    UIScore.Score(score, comb);
    //}

    //private void Awake()
    //{
    //    UIScore = GetComponent<ILS_UIScore>();
    //}

    public virtual void Initialize()
    {
        UIScore = GetComponent<ILS_UIScore>();
    }

    private float score = 0, comb = 0, comb1 = 0;

    public virtual void PlusScore()
    {
        comb1++;
        score += 10;
        Debug.Log(score + " Score");
        //Debug.Log(comb + " Comb");
        UIScore.Score(score);
    }
    public virtual void MinusScore()
    {
        comb=0;
        //Debug.Log(comb + " Comb");
    }
}

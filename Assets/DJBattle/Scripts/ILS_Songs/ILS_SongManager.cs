﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ILS_SongManager : MonoBehaviour
{
    public AudioSource song;
    public AudioSource Enviroment;
    private ILS_ConsoleBehavior console;
    public ILS_Button[] button;
    public ILS_Disc[] disc;

    private void Awake()
    {
        console = GetComponent<ILS_ConsoleBehavior>();
        button = GetComponentsInChildren<ILS_Button>();
        disc = GetComponentsInChildren<ILS_Disc>();

    }

    public void StartSong()
    {
        song.Play();
        StartCoroutine(SongTime());
    }

    public void SendAnimation(float lenght1, float lenght2, float lenght3, float lenght4, float lenght5, float lenght6)
    {
            button[0].ActiveAnimation(lenght1);
            button[1].ActiveAnimation(lenght2);
            button[2].ActiveAnimation(lenght3);
            button[3].ActiveAnimation(lenght4);
            button[4].ActiveAnimation(lenght5);
            button[5].ActiveAnimation(lenght6);
    }

    //public void SendAnimationDisc()
    //{
    //    for (int i = 0; i < button.Length; i++)
    //    {
    //        disc[i].ActiveRotateAnimation(Vector3.up, 1f,-50f);
    //    }
    //}

    IEnumerator SongTime()
    {
        while (Mathf.Round(song.time * 1) / 1 < 505)
        {
            //Debug.Log(Mathf.Round(song.time * 1) / 1);
            if ((Mathf.Round(song.time * 1 / 1) == 2))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 4))
            {
                SendAnimation(0f, 1f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 6))
            {
                SendAnimation(0f, 0f, 1f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 8))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 10))
            {
                SendAnimation(0f, 0f, 0f, 0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 12))
            {
                SendAnimation(0f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 14))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 16))
            {
                SendAnimation(0f, 1.5f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 18))
            {
                SendAnimation(0f, 0f, 1.5f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 20))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 22))
            {
                SendAnimation(0f,0f,0f, 1f, 1.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 24))
            {
                SendAnimation(1f, 0f, 0f, 0f, 1.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 26))
            {
                SendAnimation(1.5f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 28))
            {
                SendAnimation(1.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 30))
            {
                SendAnimation(1.5f, 1f, 2f,0f, 0f, 0f );
            }
            if ((Mathf.Round(song.time * 1 / 1) == 32))
            {
                SendAnimation(2f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 34))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 36))
            {
                SendAnimation(0f, 0f, 0f, 2f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 38))
            {
                SendAnimation(1.5f, 1f, 2f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 40))
            {
                SendAnimation(2f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 42))
            {
                SendAnimation(1.5f, 1f, 0f, 2f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 44))
            {
                SendAnimation(0f, 0f, 2f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 46))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 2f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 48))
            {
                SendAnimation(0f, 2f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 50))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 2f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 52))
            {
                SendAnimation(0f, 2f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 54))
            {
                SendAnimation(2f, 0f, 0f, 1.5f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 56))
            {
                SendAnimation(05f, 1f, 1.5f, 0f, 2f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 58))
            {
                SendAnimation(1.5f, 1f, 0f, 2f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 60))
            {
                SendAnimation(0f, 0f, 2f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 62))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 64))
            {
                SendAnimation(0f, 1f, 1.5f, 0f, 2f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 66))
            {
                SendAnimation(1.5f, 1f, 2f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 68))
            {
                SendAnimation(0f, 0f, 2f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 70))
            {
                SendAnimation(1.5f, 1f, 0f, 2f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 72))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 2f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 74))
            {
                SendAnimation(1.5f, 1f, 2f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 76))
            {
                SendAnimation(2f, 1f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 78))
            {
                SendAnimation(2f, 0f, 0f, 1f, 1.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 80))
            {
                SendAnimation(1.5f, 1f, 2f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 82))
            {
                SendAnimation(0f, 0f, 1f, 2f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 84))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 86))
            {
                SendAnimation(1f, 0f, 0f, 2f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 88))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 90))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 92))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 94))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 96))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 98))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 100))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 102))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 104))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 106))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 108))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 110))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 112))
            {
                SendAnimation(0.5f, 1f, 0f, 0.5f, 0f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 114))
            {
                SendAnimation(0f, 1f, 0f, 1.5f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 116))
            {
                SendAnimation(1.5f, 0f, 0.5f, 2f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 118))
            {
                SendAnimation(0f, 1.5f, 0f, 2f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 120))
            {
                SendAnimation(1f, 2f, 0.5f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 122))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 124))
            {
                SendAnimation(0f, 0f, 0.5f, 0f, 1.5f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 126))
            {
                SendAnimation(1.5f, 1f, 0f, 2f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 128))
            {
                SendAnimation(0.5f, 1f, 0f, 0.5f, 0f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 130))
            {
                SendAnimation(2f, 0f, 0.8f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 132))
            {
                SendAnimation(0f, 0f, 1f, 0f, 0.5f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 134))
            {
                SendAnimation(1.5f, 1f, 0f, 0.5f, 0f, 2f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 136))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 138))
            {
                SendAnimation(0f, 2f, 0.5f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 140))
            {
                SendAnimation(0.5f, 0f, 0f, 1f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 142))
            {
                SendAnimation(0f, 1.5f, 0f, 1f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 144))
            {
                SendAnimation(0f, 1f, 1.5f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 146))
            {
                SendAnimation(0.5f, 1f, 0.5f, 0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 148))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 150))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 152))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 154))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 156))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 158))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 160))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 162))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 164))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 166))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 168))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 170))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 172))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 174))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 176))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 178))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 180))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 182))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 184))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 186))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 188))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 190))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 192))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 194))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 196))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 198))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 200))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 202))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 204))
            {
                SendAnimation(0f, 1f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 206))
            {
                SendAnimation(0f, 0f, 1f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 208))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 210))
            {
                SendAnimation(0f, 0f, 0f, 0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 212))
            {
                SendAnimation(0f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 214))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 216))
            {
                SendAnimation(0f, 1.5f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 218))
            {
                SendAnimation(0f, 0f, 1.5f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 220))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 222))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 224))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 226))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 228))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 230))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 232))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 234))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 236))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 238))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 240))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 242))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 244))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 246))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 248))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 250))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 252))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 254))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 26))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 258))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 260))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 262))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 264))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 266))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 268))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 270))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 272))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 274))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 276))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 278))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 280))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 282))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 284))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 286))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 288))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 290))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 292))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 294))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 296))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 298))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 300))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 302))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 304))
            {
                SendAnimation(0f, 1f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 306))
            {
                SendAnimation(0f, 0f, 1f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) ==308))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 310))
            {
                SendAnimation(0f, 0f, 0f, 0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 312))
            {
                SendAnimation(0f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 314))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 316))
            {
                SendAnimation(0f, 1.5f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 318))
            {
                SendAnimation(0f, 0f, 1.5f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 320))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 322))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 324))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0.5f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 326))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 328))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 330))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 332))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 334))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 336))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 338))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 340))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 342))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 344))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 346))
            {
                SendAnimation(1.5f, 1f, 0.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 348))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 350))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 352))
            {
                SendAnimation(0f, 0f, 0f, 0.5f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 354))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 356))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 358))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 360))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 362))
            {
                SendAnimation(0f, 0f, 0f, 1.5f, 1f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 364))
            {
                SendAnimation(0.5f, 1f, 1.5f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 366))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 368))
            {
                SendAnimation(0.5f, 0f, 0f, 0f, 1f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 370))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 372))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 374))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 376))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 378))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 380))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 382))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 384))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 386))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 388))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 390))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 392))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 394))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 396))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 398))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 400))
            {
                SendAnimation(1.5f, 1f, 0f, 0f, 0f, 0.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 402))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 404))
            {
                SendAnimation(0f, 1f, 0f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 406))
            {
                SendAnimation(0f, 0f, 1f, 0f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 408))
            {
                SendAnimation(0f, 0f, 0f, 1f, 0f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 410))
            {
                SendAnimation(0f, 0f, 0f, 0f, 1f, 0f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 412))
            {
                SendAnimation(0f, 0f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 414))
            {
                SendAnimation(1f, 0f, 0f, 0f, 0f, 1.5f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 416))
            {
                SendAnimation(0f, 1.5f, 0f, 0f, 0f, 1f);
            }
            if ((Mathf.Round(song.time * 1 / 1) == 418))
            {
                SendAnimation(0f, 0f, 1.5f, 0f, 0f, 1f);
            }
            yield return new WaitForSeconds(1);
        }
        SceneManager.LoadScene("Pruebas");
    }
}

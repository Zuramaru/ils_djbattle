﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ILS_MainBehaviour : Sample4_UIBase
{
    private Transform parentView;

    public void OpenViewLevel()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.LevelSelector, UIState.Open);
    }

    public void OpenHUD()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.HUD, UIState.Open);
    }

    public void OpenSettings()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.Settings, UIState.Open);
    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
        CloseView();
    }
    public override void CloseView()
    {
        base.CloseView();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ILS_UIScore : MonoBehaviour
{
    public Text ScoreF, CombF;

    public void Score(float score)
    {
        ScoreF.text = score.ToString();
    }
}

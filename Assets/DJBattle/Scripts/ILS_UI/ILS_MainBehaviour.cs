﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sample4_MainBehaviour : Sample4_UIBase
{
    private Transform parentView;
    private Sample4_UIManager UI;

    private void Awake()
    {
        UI = GetComponent<Sample4_UIManager>();
    }

    public void LoadScenes()
    {
        Debug.Log("Cambiar escena");
        SceneManager.LoadScene("AudioVR");
    }

    public void OpenViewLevel()
    {
        parentView = GetComponentInChildren<RectTransform>().transform.parent;
        ChangeView(UIView.LevelSelector, UIState.Open);
    }

    public void OpenHUD()
    {
        UI.OpenHud();
        CloseView();
    }

    public void OpenSettings()
    {

    }

    public void ChangeView(UIView view, UIState viewState)
    {
        Sample4_FactoryView.CreateView(view, parentView);
        CloseView();
    }
    public override void CloseView()
    {
        base.CloseView();
    }

}

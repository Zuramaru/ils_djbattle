﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sample4_UIManager : MonoBehaviour
{

    private GameObject child;

    private void Awake()
    {
        child = transform.GetChild(1).gameObject;
    }

    public void OpenHud()
    {
        child.SetActive(true);
    }
}
